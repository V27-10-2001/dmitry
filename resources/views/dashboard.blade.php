<x-app-layout>
    <link rel="stylesheet" type="text/css" href="css/style1.css" >
    <x-slot name="header">
        <h2 align="center" class="font-semibold text-xl text-gray-800 leading-tight">
        Личный кабинет</h2>
    </x-slot>

    <div class="py-12">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="box4">
                        <div style="text-align: center;">
                            <h1 style="padding-top: 1%; color: #ffffff;">ЦЕНЫ</h1>
                            <h3>БУХГАЛТЕРСКИЙ УЧЁТ И АУДИТ ДЛЯ ООО И ИП.</h3>
                            <div style="display: grid; text-align: start; margin-left: 2%; margin-right: 2%; margin-top: 1%; padding-bottom: 3%; grid-template-columns: 1fr 1fr 1fr 1fr 1fr; grid-gap: 1rem;">
                                @foreach($products as $product)
                                <div style="background-color: white;;">
                                    <img style="height: 148px;" src=" {{ $product->image }} ">
                                    <div style="padding: 5%;">
                                        <h1>{{ $product->name }}</h1>
                                        <h2>{{ $product->price }}</h2>
                                        {{ $product->description }}</br>
                                        <form action="/order" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <input style="display: none;" type="text" name="user_id" value="{{ Auth::user()->id }}">
                                            <input style="display: none;" type="text" name="service_id" value="{{ $product->id }}">
                                            <input type="submit" value="Заказать" onclick="alert( 'Спасибо за заказ! В самое ближайшее время мы свяжемся с вами для согласования всех условий покупки.' );">
                                        </form>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        </div>
                    </div>
                 </div>

                </div>
    </div>
</x-app-layout>
