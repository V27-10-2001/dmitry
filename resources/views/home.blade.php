<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ФЦ «Рафинад» | Центр бухгалтерского обслуживания ООО и ИП Липецк.</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" >
</head>
<body>
<header>
    <div class="offers">
        <div class="slider">
            <div class="slider__wrapper">
                <div class="slider__items">
                    <div class="slider__item">
                        <div class="box">
                            <div>
                                <div>
                                    <img style="height: 7rem;" src="storage\image\logo.svg">
                                    <h1 style="color: #ffffff;">БЕСПЛАТНАЯ ПРОВЕРКА.</h1>
                                    <p><strong>Только в апреле!</strong> Перед тем, как начать совместную работу, наши бухгалтеры проведут бесплатный анализ Вашей бухгалтерии.</p>
                                </div>
                            </div>
                            <div></div>
                        </div>
                    </div>
                    <div class="slider__item">
                        <div class="box">
                            <div>
                                <div>
                                    <img style="height: 7rem;" src="storage\image\logo.svg">
                                    <h1 style="color: #ffffff;">БЕСПЛАТНАЯ КОНСУЛЬТАЦИЯ.</h1>
                                    <p id="1"><strong>Только в апреле!</strong> Как выбрать подходящий режим налогообложения или правильно сформировать отчетность, как устранить ошибки в документах и другие вопросы.</p>
                                </div>
                            </div>
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="js/script.js"></script>
    <div class="menu">
        <!-- Оформить меню -->
        <div class="logo">
            <img src="storage\image\logo.svg">
        </div>
        <div></div>
        <button class="btn" ><a href="#1" style="color: #ffffff; text-decoration: none;">Главная</button></a>
        <button class="btn" ><a href="#2" style="color: #ffffff; text-decoration: none;">Наши услуги</button></a>
        <button class="btn" ><a href="#3" style="color: #ffffff; text-decoration: none;">Цены</button></a>
        <button class="btn" ><a href="#4" style="color: #ffffff; text-decoration: none;">Контакты</button></a>
        <div></div>
        <button class="btn auth">Регистрация</button>
        <button class="btn auth">Вход</button>
    </div>
</header>
<main>
    <div class="box1">
        <div style="text-align: center;">
            <h1 style="padding-top: 1%; color: #ffffff;">РЕШАЕМ ЛЮБЫЕ ЗАДАЧИ</h1>
            <h3 id="2">МЫ РАБОТАЕМ ТОЛЬКО С АТТЕСТОВАННЫМИ СПЕЦИАЛИСТАМИ ВЫСШЕЙ КАТЕГОРИИ</h3>
            <div style="display: grid; text-align: start; margin-left: 15%; margin-right: 15%; margin-top: 1%; padding-bottom: 3%; grid-template-columns: 5fr 5fr 5fr; grid-gap: 2rem;">
                <div style="background-color: white;;">
                    <img style="height: 200px;" src="storage\image\1(1).jpg">
                    <div style="padding: 5%;">
                        <h1>Консалтинг</h1>
                        <li> Налоговое консультирование<br></li>
                        <li> Рекрутинг<br></li>
                        <li> Аудиторские услуги<br></li>
                        <li>Решение нестандартных задач<br></li>
                    </div>
                </div>
                <div style="background-color: white;;">
                    <img style="height: 200px;" src="storage\image\2(1).jpg">
                    <div style="padding: 5%;">
                        <h1>Аутсорсинг</h1>
                        <li> Бухгалтерское обслуживание<br></li>
                        <li> Постановка бухгалтерского учета<br></li>
                        <li>Восстановление бухгалтерского учета<br></li>
                        <li>Ведение участков бухгалтерии<br></li>
                        <li>Расчет зарплаты<br></li>
                    </div>
                </div>
                <div style="background-color: white;;">
                    <img style="height: 200px;" src="storage\image\3(1).jpg">
                    <div style="padding: 5%;">
                        <h1>Рекрутинг и КДП</h1>
                        <li> Подбор бухгалтеров<br></li>
                        <li> Функция «Главный бухгалтер»<br></li>
                        <li>Ведение кадрового делопроизводства<br></li>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box2">
        <div style="text-align: center;">
            <h1 style="padding-top: 1%;">НАЧАТЬ РАБОТАТЬ С НАМИ ПРОСТО</h1>
            <h3>ВСЕГО НЕСКОЛЬКО ШАГОВ И ВЫ БУДЕТЕ СПОКОЙНЫ ЗА ВАШУ БУХГАЛТЕРИЮ.</h3>
            <div style="display: grid; text-align: start; margin-left: 15%; margin-right: 15%; margin-top: 1%; padding-bottom: 3%; grid-template-columns: 5fr 5fr 5fr 5fr 5fr 5fr; grid-gap: 10px;">
                <div style="background-color: white;;">
                    <img style="height: 82.5px;" src="storage\image\1(2).png">
                    <div style="padding: 5%;">
                        <h1>Заявка</h1>
                        Вы оставляете заявку на нашем сайте. Наш специалист связывается с Вами и приглашает Вас на встречу.<br>
                    </div>
                </div>
                <div style="background-color: white;;">
                    <img style="height: 82.5px;" src="storage\image\2(2).png">
                    <div style="padding: 5%;">
                        <h1>Встреча</h1>
                        Мы обсуждаем текущую ситуацию в Вашей компании, виды и объемы операций и предлагаем вам варианты решения Ваших задач.<br>
                    </div>
                </div>
                <div style="background-color: white;;">
                    <img style="height: 82.5px;" src="storage\image\4(2).png">
                    <div style="padding: 5%;">
                        <h1>Договор</h1>
                        Мы подписываем договор и соглашение о конфиденциаль-ности, которые гарантируют сохранность Вашей информации.<br>
                    </div>
                </div><div style="background-color: white;;">
                    <img style="height: 82.5px;" src="storage\image\5(2).png">
                    <div style="padding: 5%;">
                        <h1>Документы</h1>
                        Мы принимаем от Вас документы и определяем график работы.
                    </div>
                </div><div style="background-color: white;;">
                    <img style="height: 82.5px;" src="storage\image\6(2).png">
                    <div style="padding: 5%;">
                        <h1>Работа</h1>
                        Мы берем вашу компанию на полное бухгалтерское обслуживание и экономим Ваши деньги и время.
                    </div>
                </div>
                <div style="background-color: white;;">
                    <img style="height: 82.5px;" src="storage\image\3(2).png">
                    <div style="padding: 5%;">
                        <h1>Результат</h1>
                        Полный порядок в Вашей бухгалтерской и налоговой документации и вовремя сданные отчеты.<br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box3">
        <div style="text-align: center;">
            <h1 style="padding-top: 1%;">КЛИЕНТАМ С НАМИ ВЫГОДНО</h1>
            <h3>МЫ НЕПРЕРЫВНО ОБУЧАЕМ И ЭКЗАМЕНУЕМ СПЕЦИАЛИСТОВ ЗА СВОЙ СЧЕТ.</h3>
            <div style="display: grid; text-align: start; margin-left: 15%; margin-right: 15%; margin-top: 1%; padding-bottom: 3%; grid-template-columns: 5fr 5fr 5fr; grid-gap: 2rem;">
                <div style="background-color: white;;">
                    <img style="height: 82.5px;" src="storage\image\К(1).png" >
                    <div style="padding: 5%;">
                        <h1> Уменьшение затрат на бугалтерию</h1>
                    </div>
                </div>
                <div style="background-color: white;;">
                    <img style="height: 82.5px;" src="storage\image\К(2).png" >
                    <div style="padding: 5%;">
                        <h1>Работа напрямую с главным бухгалтером.</h1>
                    </div>
                </div><div style="background-color: white;;">
                    <img style="height: 82.5px;" src="storage\image\K(3).png" >
                    <div style="padding: 5%;">
                        <h1>Полная безопасность ваших данных</h1>
                    </div>
                </div><div style="background-color: white;;">
                    <img style="height: 82.5px;" src="storage\image\К(4).png" >
                    <div style="padding: 5%;">
                        <h1>Возможность контроля работы в режиме ON-LiNE</h1>
                    </div>
                </div>
                <div style="background-color: white;;">
                    <img style="height: 82.5px;" src="storage\image\К(5).png" >
                    <div style="padding: 5%;">
                        <h1>Гарантии, в соответствие с условиями договора.</h1>
                    </div>
                </div>
                <div style="background-color: white;;">
                    <img style="height: 82.5px;" src="storage\image\К(6).png" >
                    <div style="padding: 5%;">
                        <h1>Наша полная финансовая ответственность.</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box4">
        <div style="text-align: center;">
            <h1 style="padding-top: 1%; color: #ffffff;">ЦЕНЫ</h1>
            <h3 id="3">БУХГАЛТЕРСКИЙ УЧЁТ И АУДИТ ДЛЯ ООО И ИП.</h3>
            <div style="display: grid; text-align: start; margin-left: 2%; margin-right: 2%; margin-top: 1%; padding-bottom: 3%; grid-template-columns: 1fr 1fr 1fr 1fr 1fr; grid-gap: 1rem;">
                @foreach($products as $product)
                <div style="background-color: white;;">
                    <img style="height: 148px;" src=" {{ $product->image }} ">
                    <div style="padding: 5%;">
                        <h1>{{ $product->name }}</h1>
                        <h2>{{ $product->price }}</h2>
                        {{ $product->description }}</br>
                    </div>
                    <a style="width: 100%;" class="btn2" href="/dashboard">Заказать</a>
                </div>
            @endforeach
            </div>
        </div>
    </div>
    </div>
    </div>
    <div class="box5">
        <div style="text-align: center;">
            <h1 style="padding-top: 1%;">РЕШЕНИЕ</h1>
            <h3>НАШ ПОДХОД К БУХГАЛТЕРИИ.</h3>
            <div style="display: grid; text-align: start; margin-left: 7%; margin-right: 7%; margin-top: 1%; padding-bottom: 3%; grid-template-columns: 5fr 5fr 5fr 5fr; grid-gap: 2rem;">
                <div style="background-color: rgba(255, 255, 255, 0.5);">
                    <img style="height: 160px;" src="storage\image\r(1).jpg">
                    <div style="padding: 5%;">
                        <h1>Документы</h1>
                        Мы разбираемся и приводим в порядок документы. При необходимости, мы восстановим Ваш бухгалтерский учет.<br>
                    </div>
                </div>
                <div style="background-color: rgba(255, 255, 255, 0.5);">
                    <img style="height: 160px;" src="storage\image\r(2).jpg">
                    <div style="padding: 5%;">
                        <h1>Отчеты</h1>
                        Мы отчитываемся перед государством за Вас. Собираем первичную бухгалтерскую документацию, обрабатываем, составляем и сдаем отчеты в налоговую.<br>
                    </div>
                </div>
                <div style="background-color: rgba(255, 255, 255, 0.5);">
                    <img style="height: 160px;" src="storage\image\r(3).jpg">
                    <div style="padding: 5%;">
                        <h1>Налоги</h1>
                        Мы решаем все вопросы с налогами. Проводим аудит Вашей текущей системы налогообложения и законную оптимизацию налоговых обязательств.<br>
                    </div>
                </div>
                <div style="background-color: rgba(255, 255, 255, 0.5);">
                    <img style="height: 160px;" src="storage\image\r(4).jpg">
                    <div style="padding: 5%;">
                        <h1>Кадры</h1>
                        Мы решаем вопрос с персоналом. Устраиваем сотрудников в штат, начисляем им зарплату, учитываем рабочее время, ведем кадровую документацию.<br>
                    </div>
                </div>
            </div>
        </div>
        <div class="box6">
            <div style="text-align: center;">
                <h1 style="padding-top: 1%;">БЕЗОПАСНОСТЬ</h1>
                <h3>ГАРАНТИЯ БЕЗОПАСНОСТИ ВАШИХ ДАННЫХ</h3>
                <div style="display: grid; text-align: start; margin-left: 7%; margin-right: 7%; margin-top: 1%; padding-bottom: 3%; grid-template-columns: 5fr 5fr 5fr 5fr; grid-gap: 2rem;">
                    <div style="background-color: rgba(255, 255, 255, 0.5);">
                        <img style="height: 182px;" src="storage\image\b(1).jpg">
                        <div style="padding: 5%;">
                            <h1 style="text-shadow: rgba(0, 0, 0, 0.5) 1px 0 0px, rgb(0, 0, 0, 0.5) 0 1px 0px, rgb(0, 0, 0, 0.5) -1px 0 0px, rgb(0, 0, 0, 0.5) 0 -1px 0px;">Надежность</h1>
                            Крупнейший в России дата-центр. Строгий контроль доступа. Соответствие высоким уровням безопасности.<br>
                        </div>
                    </div>
                    <div style="background-color: rgba(255, 255, 255, 0.5);">
                        <img style="height: 182px;" src="storage\image\b(2).jpg">
                        <div style="padding: 5%;">
                            <h1 style="text-shadow: rgba(0, 0, 0, 0.5) 1px 0 0px, rgb(0, 0, 0, 0.5) 0 1px 0px, rgb(0, 0, 0, 0.5) -1px 0 0px, rgb(0, 0, 0, 0.5) 0 -1px 0px;">Контроль доступа</h1>
                            24х7 Охрана помещений. Система охранного телевидения. Биометрическая идентификация.<br>
                        </div>
                    </div>
                    <div style="background-color: rgba(255, 255, 255, 0.5);">
                        <img style="height: 182px;" src="storage\image\b(4).jpg">
                        <div style="padding: 5%;">
                            <h1 style="text-shadow: rgba(0, 0, 0, 0.5) 1px 0 0px, rgb(0, 0, 0, 0.5) 0 1px 0px, rgb(0, 0, 0, 0.5) -1px 0 0px, rgb(0, 0, 0, 0.5) 0 -1px 0px;">Оборудование</h1>
                            Система кондиционирования. Система сверхраннего обнаружения дыма<br>
                        </div>
                    </div>
                    <div style="background-color: rgba(255, 255, 255, 0.5);">
                        <img style="height: 182px;" src="storage\image\b(5).jpg">
                        <div style="padding: 5%;">
                            <h1 style="text-shadow: rgba(0, 0, 0, 0.5) 1px 0 0px, rgb(0, 0, 0, 0.5) 0 1px 0px, rgb(0, 0, 0, 0.5) -1px 0 0px, rgb(0, 0, 0, 0.5) 0 -1px 0px;">Электроснабжение</h1>
                            Надежная система электроснабжения. Дизель-роторные источники бесперебойного питания<br>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box7">
                <div style="padding: 2%;">
                    <h1 style=" color: #000000;">Координаты</h1>
                    <h3 id="4">Будем рады видеть Вас у нас:</h3>
                    <hr>
                    <h4><b>ФЦ «Рафинад»</b></h4>
                    <h4>  г.Липецк, .</h4>
                    <h4>  +7 (952) 592-66-92.</h4>
                </div>
            </div>
</main>
<footer>
    <div style="text-align: center; color: #fff8f8;">
        © 2021. «Рафинад»<br>
        г.Липецк, , тел.: +7 (952) 592-66-92.<br>
        Все права защищены.
    </div>
</footer>
</body>
</html>
