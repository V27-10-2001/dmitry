<?php

use Illuminate\Support\Facades\Route;
use App\Models\Services;
use Illuminate\Routing\RouteGroup;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    $products = Services::all();
    return view('home', compact('products'));
});

Route::namespace("App\Http\Controllers")->group(function () {
    Route::resource('panel', ServicesController::class);
    Route::post('/order', 'ServicesController@order');
});


Route::get('/dashboard', function () {
    $products = Services::all();
    return view('dashboard', compact('products'));
})->middleware(['auth'])->name('dashboard');


require __DIR__.'/auth.php';
