<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Services extends Model{
    use HasFactory;

    protected $table = 'services';

    public $fillable = [
        'name',
        'price',
        'description',
        'image',
    ];
   public function getImageAttribute($value) {
       if (is_null($value)) {
           return null;
       } else {
           return url('storage/image/' . $value);
       }
   }
}
