<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Services;
use App\Models\UserService;
use App\Http\Resources\UserServiceResource;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_products = UserService::all();
        foreach ($user_products as $user_product)
        {
            $user_product->service_name = Services::findOrFail($user_product->service_id)->name;
            $user_product->user_mail = User::findOrFail($user_product->user_id)->email;
            $user_product->user_name = User::findOrFail($user_product->user_id)->name;
        }
        $products = Services::all();
        return view('panel', compact('products', 'user_products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_products = UserService::all();
        foreach ($user_products as $user_product)
        {
            $user_product->service_name = Services::findOrFail($user_product->service_id)->name;
            $user_product->user_mail = User::findOrFail($user_product->user_id)->email;
            $user_product->user_name = User::findOrFail($user_product->user_id)->name;
        }
        $products = Services::all();
        return view('panel', compact('products', 'user_products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $file_name = Str::random(27) . "." . $request->image->extension();

        Storage::disk('public')->putFileAs('image', $request->image, $file_name);

        Services::create([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
            'image' => $file_name,
        ]);

        return redirect('/panel');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $products = Services::findOrFail($id);

        if ($request->image){
            $file_name = Str::random(27) . "." . $request->image->extension();

            Storage::disk('public')->putFileAs('image', $request->image, $file_name);

            $products->image = $file_name;
        }

        $products->name = $request->name;
        $products->price = $request->price;
        $products->description = $request->description;

        $products->save();

        return redirect("/panel/$id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
           $products = Services::findOrFail($id);

        $products->delete();

        return redirect('/panel');
    }

    public function order(Request $request)
    {

        UserService::create([
            'user_id' => $request->user_id,
            'service_id' => $request->service_id,
        ]);

        return redirect('/dashboard');
    }
}
